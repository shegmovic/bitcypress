const fs = require('fs-extra')

const path = require('path')

module.exports = (on, config) => {​​

  // `on` is used to hook into various events Cypress emits

  // `config` is the resolved Cypress config

    // accept a configFile value or use development by default

  function processConfigName(on, config) {​​

    const file = config.env.name || "default"

    return getConfigFile(file).then(function(file) {​​

 

      return file;

    }​​)   

  }​​

 

  function getConfigFile(file) {​​

    const pathToConfigFile = path.resolve('cypress', 'config', file+'.json')

    return fs.readJson(pathToConfigFile)

  }​​

  return processConfigName(on, config);

 

}​​