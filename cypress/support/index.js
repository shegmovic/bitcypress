// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************




// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

const fs = require('fs-extra')
const path = require('path')
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
    // accept a configFile value or use development by default
  function processConfigName(on, config) {
    const file = config.env.name || "default"
    return getConfigFile(file).then(function(file) {
 
      return file;
    })   
  }
 
  function getConfigFile(file) {
    const pathToConfigFile = path.resolve('cypress', 'config', file+'.json')
    return fs.readJson(pathToConfigFile)
  }
  return processConfigName(on, config);
 
}
